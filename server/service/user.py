"""
    The service is an abstraction layer between the route-handler and the database
    This is a common pattern in other languagues, I'm still unsure whether it fits for
    Python and Flask.

    The idea is to create more generic functions for a specific domain of usage
    (such as the user in this case). The intent is to avoid repeated code if
    a certain db call is required in multiple routes.

    All operations should be done via the model (in this case the User).
    See http://flask-sqlalchemy.pocoo.org/2.3/ for more information
    regarding what operations are available.
"""

from server.model.user import User
from server.database import db


def get_user(id):
    user = User.query.get(id)
    return user


def get_list():
    users = User.query.all()
    return users


def add_user(login, password):
    # Create a new user, given the info.
    new_user = User(login=login, password=password)

    # Add it to the session, this prepares the SQL statements under the hood.
    db.session.add(new_user)

    # Nothing is actually created in the database until we commit.
    db.session.commit()
    return new_user
