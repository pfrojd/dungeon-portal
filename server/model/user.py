from datetime import datetime
from server.database import db


class User(db.Model):
    __tablename__ = "user"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    login = db.Column(db.String())
    password_hash = db.Column(db.String())
    created = db.Column(db.DateTime, server_default=db.func.now())

    # constructor - should exist for all models
    def __init__(self, login, password):
        self.login = login
        self.password_hash = password
        self.created = datetime.now()

    # this is something required by sqlalchemy, but it should be the same
    # for all models, regardless of how they look
    def __repr__(self):
        return "<id {}>".format(self.id)

    # it blows my mind that python does not have native support to convert
    # a class-object to json, so apparently we have to do it ourselves
    def to_json(self):
        return {"id": self.id, "login": self.login, "created": self.created}
