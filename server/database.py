"""
    Looks rather empty in here, but it should fill out.
    
    Whenever you need to interact with the database (f.e. through a service)
    you should be importing the db object from this file.
"""

from flask_sqlalchemy import SQLAlchemy

options = {"expire_on_commit": False}

db = SQLAlchemy(session_options=options)
