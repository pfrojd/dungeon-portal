# -*- coding: utf-8 -*-
from flask import Flask, jsonify, render_template
from server.database import db
from server.util.exceptions import BadRequest, EntityDoesNotExist
from server.api.generator import generator_app
from server.api.character import character_app
from server.api.user import user_app


def create_app(active_config):
    app = Flask(
        __name__, static_folder="../app/dist/static", template_folder="../app/dist"
    )

    # read the config file
    app.config.from_object(active_config)

    db.init_app(app)

    # split each "part" of the application into what flask calls a blueprint
    # essentially, its just a way of modularizing the different parts of the application
    app.register_blueprint(generator_app)
    app.register_blueprint(character_app)
    app.register_blueprint(user_app)

    register_handlers(app)

    # Serve HTML from here
    @app.route("/", defaults={"path": ""})
    @app.route("/<path:path>")
    def catch_all(path):
        return render_template("index.html")

    return app


def register_handlers(app):
    """
        In order to get Flask to properly react to our exceptions
        we have to register them in the flask errorhandler.
    """

    # Don't use this yet.
    #
    # @app.errorhandler(Exception)
    # def handle_generic_error(error):
    #     """
    #         This error handler is here to ensure that if we at any point
    #         have a generic exception, we should at least have some sort of response.
    #     """
    #     response = jsonify({"error": "Something went wrong"})
    #     response.status_code = 500
    #     return response

    @app.errorhandler(BadRequest)
    def handle_bad_request(error):
        response = jsonify(error.to_dict())
        response.status_code = error.status_code
        return response

    @app.errorhandler(EntityDoesNotExist)
    def handle_entity_does_not_exist(error):
        response = jsonify(error.to_dict())
        response.status_code = error.status_code
        return response
