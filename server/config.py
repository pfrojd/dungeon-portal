# -*- coding: utf-8 -*-
"""
This is the configuration file for the application
This contains various flags and other stuff that's used by the frameworks we use.

It should not contain any sensitive information, passwords, usernames
and other database information should be added to the .env file.
"""

import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    # flask core settings
    DEBUG = False
    TESTING = False
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URI']
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class ProductionConfig(Config):
    # Production settings (a.k.a deployed settings) go here
    DEBUG = False


class DevelopmentConfig(Config):
    DEBUG = True
    DEVELOPMENT = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
