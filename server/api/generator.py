import random
from flask import Blueprint
from random import choice
import json

# set up the blueprint for this module
generator_app = Blueprint(
    "generator",  # name of the blueprint
    __name__,
    url_prefix="/api/generator",  # URL prefix for this 'component'
)


@generator_app.route("/name")
def random_name():
    randomness = random.randint(1, 2)
    print("Your random number was %s" % randomness)
    if randomness == 1:
        return "Poop"
    else:
        return "Rat"


@generator_app.route("/item")
def random_item():
    print("generating new item")

    # get the data from the json file
    item_properties = open("app/item_gen_properties.json", "r")

    # parse it
    data = json.load(item_properties)

    """
    The following lists define different properties that an item can have.
    The second value in each pair is either a flat number (whole integer) that
    dictates the base value of the item, or a float that acts as a value modifier.

    For example, a poor iron dagger's gold value would be (100+25) * .5 = 62.50
    """

    # Now we can just randomly select our items
    new_item = {}
    new_item["quality"] = choice(data["qualities"])
    new_item["material"] = choice(data["materials"])
    new_item["enchantment"] = choice(data["enchantments"])
    new_item["weapon_type"] = choice(data["weapon_types"])
    new_item["augment"] = choice(data["augments"])

    # # the base value of the item is set as follows:

    # # the raw material and the quality of it's forging affects it's value
    new_item["gold_value"] = (
        new_item["material"]["baseValue"] * new_item["quality"]["valueModifier"]
    )
    # # the weapon type affects the cose (more complex weapons require more work)
    new_item["gold_value"] += new_item["weapon_type"]["baseValue"]

    # # ennchantments and augments modify the cost as a combined modifier
    modifier = (
        new_item["enchantment"]["valueModifier"] + new_item["augment"]["valueModifier"]
    )

    new_item["gold_value"] *= modifier

    return json.dumps(new_item)
