from datetime import datetime
from flask import Blueprint, jsonify

character_app = Blueprint(
    "character",  # Name of the blueprint
    __name__,
    url_prefix="/api/character",  # URL prefix for this 'component'
)


@character_app.route("/")
def hello_world():
    return "Hello World!"


@character_app.route("/new")
def random_name():
    return jsonify(name="Poopoticalamanca", level=-1, created=datetime.now())
