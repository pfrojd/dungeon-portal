from flask import Blueprint, request, jsonify
from server.service.user import add_user, get_list, get_user
from server.util.exceptions import BadRequest, EntityDoesNotExist


# set up the blueprint for this module
user_app = Blueprint(
    "user",  # name of the blueprint
    __name__,
    url_prefix="/api/user",  # URL prefix for this 'component'
)


@user_app.route("/add", methods=["POST"])
def new_user():
    login_param = request.form.get("login")
    password_param = request.form.get("password")

    if not login_param or not password_param:
        raise BadRequest("Missing form parameters")

    new_user = add_user(login_param, password_param)
    return jsonify(new_user.to_json())


@user_app.route("/")
def get_all_users():
    users = get_list()

    # This is weird as hell syntax, but essentially we're iterating over each of the
    # users, calling .to_json and then shoving each back into a list
    return jsonify([user.to_json() for user in users])


@user_app.route("/<id>")
def get_user_by_id(id):
    """
        <id> is a way of binding that part of the URL into a variable.
        ie. if you post to /1234, id would be = 1234

        Since we don't know if we have the user with id = 1234
        we null check the value returned by the service.
    """
    user = get_user(id)
    if user is None:
        raise EntityDoesNotExist("User does not exist")
    return jsonify(user.to_json())
