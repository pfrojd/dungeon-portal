"""
    Managing Migrations

    This is not part of the application, but is something executed
    from commandline in order to manage database migrations.

    $ python manage.py db migrate
    Will create a new version of the database, given the available models.
    Note that this file has to import a model in order to have the migrations read it.

    $ python manage.py db upgrade
    Will upgrade your local database to the latest version, running each of the migrations.

    $ python manage.py db downgrade
    Will downgrade your local database to the previous version.
    
    Example:

    Adding table X to the database.
    1. Create a new model for the table, and define all columns (see other models for examples)
    2. Import the new model into this file.
    3. Run the migrate command
    4. Run the upgrade command.
    5. You now have your new table.
"""

import os
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from flask import Flask
from dotenv import load_dotenv

# activate dotenv which will read our .env file with secrets
load_dotenv()

from server.database import db
from server.model import user

app = Flask(__name__)

app.config.from_object(os.environ['APP_SETTINGS'])

migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()
