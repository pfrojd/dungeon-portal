## Dungeon Portal

### Start development

We use something called Foreman to manage multiple processes at once, since we're
gonna need to start both the frontend build system and the backend server.

So we start by installing foreman

$ npm install -g foreman

Now, navigate to the root of dungeon-portal and run the following command:

$ nf start

This will start both processes at once, in one terminal, each process is prefixed
by `frontend` and `backend` to diffrentiate the output.

### Which server should I use when developing?

By default (I think, it should say in the terminal output) the development server for the frontend
should be accessible from localhost:5001. There's already a proxy set up for talking to the python server

ie. if you make a request to `/api/generator/item` it will not send a request to `localhost:5001`, but instead
send it to the real api server which is at `localhost:5000`.

The flask application will serve the Vue application if you navigate straight to `localhost:5000` but it
will only serve the "production version", a.k.a the version that gets output if you run `npm run build` in
the `/app` directory.

### If installing stuff with npm

Make sure your terminal is in the `/app` folder when doing so, we only use node stuff in there.

### Install dependencies

$ pip install -r requirements.txt

### Start the server by running

$ python run.py

### Good links

- https://docs.python-guide.org/writing/structure/
- http://flask.pocoo.org/docs/1.0/
- https://github.com/allisson/flask-example
- https://code.visualstudio.com/docs/python/tutorial-flask
- https://realpython.com/flask-by-example-part-2-postgres-sqlalchemy-and-alembic/
- https://realpython.com/token-based-authentication-with-flask/
- https://github.com/alecthomas/flask_injector
- https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-iv-database
- http://kronosapiens.github.io/blog/2014/08/14/understanding-contexts-in-flask.html
- https://github.com/sloria/cookiecutter-flask
- https://github.com/SawdustSoftware/flask-chassis
- https://github.com/mattupstate/overholt
- https://github.com/miguelgrinberg/flasky
- https://github.com/cburmeister/flask-bones
- https://github.com/cdagli/flask-api-starter

### Want to hide the **pycache** folders?

Add this to your vscode settings

```json
"files.exclude": {
    "**/*.pyc": { "when": "$(basename).py" },
    "**/__pycache__": true
  }
```

### Changing linters and formatters

The standard linter (pylint) and formatter (autopep8) was annoying, so I swapped them out.
Not everyone is required to use the same, but here are the instructions:

Put the following in your settings:

```json
  "python.linting.pylintEnabled": false,
  "python.linting.flake8Enabled": true,
  "python.linting.enabled": true,
  "python.formatting.provider": "black"
```

Visual Studio Code should then ask you if you want to install flake8, and black, or the notification should pop up once you save.
Hit yes, or alternatively run:

$ pip install flake8 black

### Parts

- Psycopg2 - Postgres Database Driver
- SQLAlchemy - Object Relational Mapping (ORM)
- Flask-SQLAlchemy - Bindings between Flask and SQLAlchemy
- Flask-Migrate - Database migrations
- pytest - Unit Tests and more (not implemented yet)

### For databases

- Install postgreSQL 10.x
  - https://www.enterprisedb.com/downloads/postgres-postgresql-downloads
- During setup, you'll be asked to set a superuser password, pick something you remember
  - It doesn't have to be complex, since it's nothing that will be used except on your local machine
- Create a new database
- Create a new user, with a password
- Run the following SQL

```sql
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO theusernameyoupicked;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO theusernameyoupicked;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public TO theusernameyoupicked;
```

Then copy the `.env.template` file and rename the new copy to just `.env`
The value for DATABASE_URI should be: `postgresql://<username>:<password>@localhost/<databasename>`

After this is done, look at `manage.py` for more instructions on how to get the database migrations running
