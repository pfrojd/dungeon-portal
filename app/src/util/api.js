import axios from "axios";

class APIClient {
  static async getItem() {
    const request = await axios.get("/api/generator/item");
    return request.data;
  }
  static async getRandomName() {
    const request = await axios.get("/api/generator/name");
    return request.data;
  }
}

export default APIClient;
