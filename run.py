#!flask/bin/python
import os

from dotenv import load_dotenv
from server.application import create_app

# activate dotenv which will read our .env file with secrets
load_dotenv()

active_config = os.environ['APP_SETTINGS']

app = create_app(active_config)
app.run()
